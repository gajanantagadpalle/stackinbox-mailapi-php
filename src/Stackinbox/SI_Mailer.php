<?php
namespace Stackinbox\Stackinbox;

use Illuminate\Http\Request;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Stream\StreamInterface;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Illuminate\Support\Facades\Validator;

class SI_Mailer{
	static $message = array();
	
	private static function validator(array $data){
		return Validator::make($data, [
            'email_to' => 'required|string|email|max:255',
            'email_toname' => 'string|max:255',
            'email_from' => 'string|email|max:255',
			'email_fromname' => 'string|string|max:255',
            'email_replyto' => 'string|email|max:255',
            'email_subject' => 'required|string|max:255',
            'email_message' => 'required'
        ]);
	}
	
	public static function getErrorMessage(){
		return self::$message;
	}

	private static function setErrorMessage($messages, $status = 0){
		self::$message = array();
		$retErrors = array("status"=>$status, "message"=>$messages);
		self::$message = $retErrors;
	}
	
	public static function send(array $data){
		$SI_AUTHKEY = env('SI_AUTHKEY', '');
		if(trim($SI_AUTHKEY) != "" AND strlen(trim($SI_AUTHKEY)) < 65){
			$validator = self::validator($data);
			if($validator->fails()){
				self::setErrorMessage($validator->messages()->all());
				return false;
			}else{
				try{
					$client = new \GuzzleHttp\Client();
					$url = 'http://app.stackinbox.com/api';
					$data["SI_AUTHKEY"] = $SI_AUTHKEY;
					$request = $client->post($url,  ['form_params'=>$data]);
					if($request->getStatusCode() == 200){
						$result = trim($request->getBody());
						$result = json_decode($result);
						if(isset($result->status) AND $result->status== 1){
							self::setErrorMessage([trim($result->message)], 1);
							return true;
						}else{
							if(isset($result->message)){
								self::setErrorMessage([trim($result->message)], 0);
							}else{
								self::setErrorMessage([trim($request->getBody())], 0);
							}
						}
					}else{
						self::setErrorMessage(["Error in contacting server"]);
					}
				}
				catch (ClientErrorResponseException $exception) {
					self::setErrorMessage([$exception->getResponse()->getBody(true)]);
				}			
			}
		}else{
			self::setErrorMessage(["Auth key is empty. Please add the SI_AUTHKEY in env file."]);
		}
		
		return false;
		
	}
}